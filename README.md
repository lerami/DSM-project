# DSM-project (Distributed shared memory)
A school project (ENSEIRB-MATMECA) :school_satchel:

## :paperclip: Subject

Developpement of a software to share virtual memory between several processus on different machines.

## :paperclip: Build the project

As a first build, you need to create the Makefile:
```
./create-project-files.sh
```

Then go in the ```build``` directory and start the compiling process by typing ```make``` in your command line.

## :paperclip: Phase 1 : Launching the processus

To run the phase 1 :
```
./dsm-phase1 machine_file executables arg1 arg2 ...
```

> machine_file is a .txt where are written the name of the machine on which we'd like to launch the processus.


## :paperclip: Phase 2 : DSM implementation
