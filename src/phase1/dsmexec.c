/*
------------------------------------------------------------------------------
DSM project (distributed shared memory) Telecom 2A - Enseirb-mmk
@ Elise Marie & David Pignato
------------------------------------------------------------------------------
*/

#include "common_impl.h"
#include "network.h"

/* variables globales */


/* le nombre de processus effectivement crees */
volatile int num_procs_creat = 0;

void usage(void)
{
  fprintf(stdout,"Usage : dsmexec machine_file executable arg1 arg2 ...\n");
  fflush(stdout);
  exit(EXIT_FAILURE);
}

void sigchld_handler(int sig)
{
  /* on traite les fils qui se terminent */
  /* pour eviter les zombies */
  int status;
  pid_t pid;

  while((pid = waitpid(-1,&status,WNOHANG)) > 0){

    num_procs_creat --;

    if(num_procs_creat == 0){
      printf("All procs have terminated.\n");
    }
  }

}


int main(int argc, char *argv[])
{
  if (argc < 3){
    usage();
  }
  else {

    printf("[dsmexec] starting dsmexec ... \n");

    pid_t pid, remote_pid;

    int num_procs, port_num, remote_port_num;
    int sock_ecoute, remote_sockfd;
    int outfd[2];
    int errfd[2];
    int i,j, status;
    int accepted;

    char machine[NAME_MAXLEN], hostname[NAME_MAXLEN], hostaddr[ADDR_MAXLEN];
    char remote_hostname[NAME_MAXLEN], remote_hostaddr[ADDR_MAXLEN];
    char buffer[BUFFLEN],tmp[BUFFLEN], tmpb[BUFFLEN], tmpa[BUFFLEN];
    char machine_file[32];
    memset(buffer,0,BUFFLEN);
    memset(tmp,0,BUFFLEN);
    char **newargv;
    // char* prog_name=argv[2];
    char *path_dsmwrap = NULL;
    char str[BUFFLEN];
    char *path_machine_file = "../../src/phase1";

    machine_t * machine_tab;
    struct sockaddr_in remote_sock_addr;
    socklen_t remote_addrlen = sizeof(remote_sock_addr);
    sigset_t mask;
    sigaddset(&mask, SIGCHLD);
    //    struct addrinfo* sock_info;
    //   memset(&sock_info,0,sizeof(sock_info));

    /* Mise en place d'un traitant pour recuperer les fils zombies*/
    struct sigaction sa;
    sa.sa_handler = &sigchld_handler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGCHLD, &sa, NULL) == -1){
      perror("sigaction");
      exit(EXIT_FAILURE);
    }

    path_dsmwrap = getcwd(str,BUFFLEN);

    // retrieving local machine ip
    gethostname(hostname,NAME_MAXLEN);
    get_hostname_ip(hostname, hostaddr);

    //  write machine names in machines and return the number of machines/processus to be create in num_procs
    sprintf(machine_file,"%s/%s",path_machine_file,argv[1]);
    num_procs = get_num_procs(machine_file);

    printf("[dsmexec] number of processus/machines : %d. \n",num_procs);

    machine_tab = malloc(num_procs*sizeof(machine_t));
    get_machine_names(machine_file, machine_tab);

    printf("[dsmexec] machines stored. \n");
    for(i = 0; i < num_procs; i++){
      printf("[dsmexec] machine %d name : %s .\n", i, machine_tab[i].name);
    }

    /* creation de la socket d'ecoute */
    /* + ecoute effective */

    sock_ecoute = create_socket(&port_num);

    printf("[dsmexec] listening sockfd : %d. \n", sock_ecoute);

    // <====================== SONS CREATION (NEW PROCESSUS) ======================>
    for(i = 0; i < num_procs ; i++) {

      printf("[dsmexec] creating son (%d/%d) ... \n", i+1, num_procs);

      strcpy(machine, machine_tab[i].name);
      /* creation du tube pour rediriger stdout et stderr */
      pipe(outfd);
      pipe(errfd);

      pid = fork();

      if(pid == -1) ERROR_EXIT("fork");

      if (pid == 0) { /* fils */
        free(machine_tab);
        printf("[dsmexec] starting redirection ... \n");
        /* redirection stdout */
        dup2(outfd[1], STDOUT_FILENO);
        close(outfd[0]);

        /* redirection stderr */
        dup2(errfd[1], STDERR_FILENO);
        close(errfd[0]);

        /* Creation du tableau d'arguments pour le ssh */
        // ssh machine dsmwrap ipaddr port prog_name=argv[2] other_args ...

        newargv = malloc((argc+4)*sizeof(char *));
        newargv[0] = "ssh";
        newargv[1] = machine;
        sprintf(tmp,"/%s/dsmwrap",path_dsmwrap);
        newargv[2] = tmp;
        newargv[3] = hostaddr;
        sprintf(tmpb,"%d",port_num);
        newargv[4] = tmpb;
        sprintf(tmpa,"/%s/%s",path_dsmwrap,argv[2]);
        newargv[5] = tmpa;
        for(i=3; i < argc-2; i++){
          newargv[i+3] = argv[i];
        }
        newargv[5+argc-2] = NULL;

        /* jump to new prog : ssh (machine_name, prog, num_port) */
        printf("[dsmexec] connecting remote machines ... \n");
        fflush(stdout);
        fflush(stderr);
        execvp("ssh",newargv);

      } else  if(pid > 0) { /* pere */
        /* fermeture des extremites des tubes non utiles */
        // printf("%s %d\n",hostaddr,port_num);
        printf("pid %i\n", pid);
        num_procs_creat++;
        close(outfd[1]);
        close(errfd[1]);

        machine_tab[i].local_info.pid = pid;
        machine_tab[i].local_info.outfd = outfd[0];
        machine_tab[i].local_info.errfd = errfd[0];
        // stocker les fd dans chaque machine => structure machine_t

      }
    }

    // <====================== ACCEPT CONNECTION + RETRIEVING INFOS ======================>
    accepted = 0;
    while(accepted < num_procs){
      memset(buffer,'\0',BUFFLEN);
      printf("[dsmexec] Accepting connection %d ... \n", accepted);

      // /* on accepte les connexions des processus dsm */
      remote_sockfd = do_accept(sock_ecoute, (struct sockaddr*)&remote_sock_addr, &remote_addrlen);
      accepted ++;
      printf("[dsmexec] Connection %d accepted : socket %d\n",accepted, remote_sockfd);
      // /*  On recupere le nom de la machine distante */
      // /* 1- d'abord la taille de la chaine */
      // /* 2- puis la chaine elle-meme */

      readline(remote_sockfd,buffer,BUFFLEN);
      sscanf(buffer,"%s %d %s %d", remote_hostname, &remote_pid, remote_hostaddr, &remote_port_num);
      // /* On recupere le pid du processus distant  */
      // /* On recupere le numero de port de la socket */
      // /* d'ecoute des processus distants */
      for(i = 0; i < num_procs; i++){
        if(strcmp(remote_hostname,machine_tab[i].name) == 0){
          printf("[dsmexec] machine name in struct %s\n", machine_tab[i].name);
          machine_tab[i].remote_info.pid = remote_pid;
          machine_tab[i].remote_info.sockfd = remote_sockfd;
          machine_tab[i].remote_info.port_num = remote_port_num;
          strcpy(machine_tab[i].remote_info.hostaddr,remote_hostaddr);
          printf("[dsmexec] filling with this infos : %d,%s\n",machine_tab[i].remote_info.port_num,machine_tab[i].remote_info.hostaddr);
        }
      }
    }
    if(accepted == num_procs){
      printf("[dsmexec] All connections accepted.\n");
    }

    // <====================== SEND RANK AND NUM PROCS TO REMOTE MACHINE ======================>

    for(i = 0; i < num_procs; i++){
      printf("[dsmexec] send rank %d \n", i);
      memset(buffer,'\0',BUFFLEN);
      /* envoi du nombre de processus aux processus dsm*/
      /* envoi des rangs aux processus dsm */
      sprintf(buffer,"%d %d", num_procs, machine_tab[i].rank);
      printf("[dsmexec] %s %d\n", buffer, machine_tab[i].remote_info.sockfd);
      sendline(machine_tab[i].remote_info.sockfd,buffer,BUFFLEN);


    }

    // <====================== SEND CONNECTION INFO ======================>
    for (i = 0; i < num_procs; i++){
      printf("[dsmexec] send connection info %d \n", i);
      memset(buffer,0,BUFFLEN);

      /* envoi des infos de connexion aux processus */
      sprintf(buffer,"%d %s", machine_tab[i].remote_info.port_num, machine_tab[i].remote_info.hostaddr);
      printf("[dsmexec] sending this infos : %s,%d,%s\n",machine_tab[i].name,machine_tab[i].remote_info.port_num,machine_tab[i].remote_info.hostaddr);
      // each machine send its information to all the process
      for (j = 0; j < num_procs; j++){
        if (j!=i){
          sendline(machine_tab[j].remote_info.sockfd,buffer,BUFFLEN);
         }
      }

      // when it's done, the machine send its own info so that dsmwrap
      // know when the machine is ready to accept other procs connection
      sendline(machine_tab[i].remote_info.sockfd,buffer,BUFFLEN);
    }
    // printf("[dsmexec] out of connection info\n");

    /* gestion des E/S : on recupere les caracteres */
    /* sur les tubes de redirection de stdout/stderr */
    /* jusqu'à ce que les 2*num_procs tubes soient fermés ie recevoir
    assez de signaux errno qui signalent la fermeture */
    status = 1;
    while(status>0){
      status = 0;
      for(i = 0; i < num_procs; i++){
          memset(buffer,0,BUFFLEN);
        if (read(machine_tab[i].local_info.outfd,buffer,BUFFLEN) != 0){

          printf("[dsmexec : %s] stdout : %s\n",machine_tab[i].name,buffer);
          if (strcmp(buffer,"\0")!=0) status++;
        }
          memset(buffer,0,BUFFLEN);
        if (read(machine_tab[i].local_info.errfd,buffer,BUFFLEN) != 0){
          printf("[dsmexec : %s] stderr : %s\n",machine_tab[i].name,buffer);
          if (strcmp(buffer,"\0")!=0) status++;
        }
      }
    }

    printf("[dsmexec] waiting for all sons ... \n");
    /* on attend les processus fils */


    for(i = 0; i < num_procs; i++){
      /* on ferme les descripteurs proprement */
      close(machine_tab[i].local_info.outfd);
      close(machine_tab[i].local_info.errfd);
    }
    printf("[dsmexec] file descriptors closed. \n");

    /* on ferme la socket d'ecoute */
    close(sock_ecoute);
    printf("[dsmexec] sockfd closed \n");
    printf("[dsmexec] exiting programm : no errors. \n");

  }
  exit(EXIT_SUCCESS);
}
