#include "common_impl.h"
#include "network.h"

int main(int argc, char **argv)
{
  /* processus intermediaire pour "nettoyer" */
  /* la liste des arguments qu'on va passer */
  /* a la commande a executer vraiment */


  int sock_ecoute, sock_interprocs, sock_procs, sock_procs_accepted;
  int status;
  int port_interprocs;
  int num_procs, rank;
  int accepted = 0, connection = 0;
  int i;

  char hostname[NAME_MAXLEN], hostaddr_launcher[ADDR_MAXLEN], hostaddr[ADDR_MAXLEN], hostaddr_procs[ADDR_MAXLEN];
  char* prog_name = argv[3];
  char buffer[BUFFLEN];
  char port_launcher[BUFFLEN];
  char port_procs[BUFFLEN];
  char ** prog_args = malloc((argc-1)*(sizeof(char*)));
  prog_args[0] = prog_name;
  for(i = 4; i<argc;i++){
    prog_args[i-3]=argv[i];
  }

  struct addrinfo *launcher_addrinfo, *rp, *rt;
  struct addrinfo *procs_addrinfo = malloc(sizeof(procs_addrinfo));
  struct sockaddr_in sock_addr;
  // struct sockaddr * launcher;
  socklen_t sock_addr_len = sizeof(sock_addr);

  strcpy(hostaddr_launcher,argv[1]);
  strcpy(port_launcher, argv[2]);

  gethostname(hostname, NAME_MAXLEN);

  printf("[dsmwrap : %s] Retrieving launcher connection info ... \n", hostname);

  get_addr_info(hostaddr_launcher, port_launcher, &launcher_addrinfo);
  // launcher = launcher_addrinfo->ai_addr;

  // char * dst = malloc(14*sizeof(char));
  // inet_ntop(launcher->sa_family,launcher->sa_data, dst , 14);
  // printf("sa_data : %s\n", dst);

  // freeaddrinfo(launcher_addrinfo);
  // init_sock_addr(sock_ecoute, &sock_addr,port_launcher);

  // <====================== CONNECTION TO LAUNCHER (DSMEXEC) ======================>
  printf("[dsmwrap : %s] Connecting to the launcher ... \n",hostname);

  /* creation d'une socket pour se connecter au */
  /* au lanceur et envoyer/recevoir les infos */
  /* necessaires pour la phase dsm_init */
  for (rp = launcher_addrinfo; rp != NULL; rp = rp->ai_next) {
      sock_ecoute = socket(rp->ai_family, rp->ai_socktype,rp->ai_protocol);
      if (sock_ecoute == -1)
        continue;
      if (connect(sock_ecoute, rp->ai_addr, rp->ai_addrlen) != -1)
          break;

      close(sock_ecoute);
  }
  //
  // do_connect(sock_ecoute, launcher_addrinfo->ai_addr, sizeof(launcher_addrinfo->ai_addr));
  printf("[dsmwrap : %s] Connected! \n",hostname);

  // <====================== CREATION SOCKET BETWEEN DSM PROCS + SENDING INFO TO THE LAUNCHER ======================>
  get_hostname_ip(hostname,hostaddr);
  printf("[dsmwrap : %s] IP : %s\n", hostname, hostaddr);

  /* Creation de la socket d'ecoute pour les */
  /* connexions avec les autres processus dsm */
  sock_interprocs = create_socket(&port_interprocs);

  printf("[dsmwrap : %s] N°port : %d \n",hostname,port_interprocs);

  /* Envoi du nom de machine, pid , port d'écoute (propagation aux machines) au lanceur */
  printf("[dsmwrap : %s] Sending infos to launcher ... \n", hostname);

  memset(buffer,'\0',BUFFLEN);

  sprintf(buffer,"%s %d %s %d", hostname, getpid(), hostaddr, port_interprocs);
  sendline(sock_ecoute, buffer,BUFFLEN);

  memset(buffer,'\0',BUFFLEN);
  readline(sock_ecoute, buffer,BUFFLEN);
  sscanf(buffer,"%d %d", &num_procs, &rank);
  printf("[dsmwrap : %s] Retrieving info from launcher : num_procs = %d, rank = %d\n", hostname, num_procs, rank);

  // <====================== LOOP TO ACCEPT/CONNECT TO OTHER MACHINES ======================>
  // two cases : 1) accepting connection from other processus
  //             2) connecting to other processus

  printf("[dsmwrap : %s] Starting to accept and connect (to) other dsm procs ...\n",hostname);
  for(i = 0; i < num_procs; i++){
    memset(buffer,0,BUFFLEN);
    readline(sock_ecoute,buffer,BUFFLEN);
    sscanf(buffer,"%s %s",port_procs, hostaddr_procs);
    printf("[dsmwrap : %s] connection to %s\n",hostname,buffer);

    if (strcmp(hostaddr,hostaddr_procs) == 0){ //accepting mode (when machine send its own addr)
      while(accepted < num_procs-1){
        sock_procs_accepted = do_accept(sock_interprocs, (struct sockaddr*)&sock_addr, &sock_addr_len);
        accepted ++;
        printf("[dsmwrap : %s] connection accepted (%d/%d)\n", hostname, accepted, num_procs-1);
      }
    }
    else { // connect to other processus
      get_addr_info(hostaddr_procs, port_procs, &procs_addrinfo);
      for (rt = procs_addrinfo; rt != NULL; rt = rt->ai_next) {
          sock_procs = socket(rt->ai_family, rt->ai_socktype,rt->ai_protocol);
          if (sock_procs == -1)
            continue;
          if (connect(sock_procs, rt->ai_addr, rt->ai_addrlen) != -1)
              break;

          close(sock_procs);
      }
      printf("[dsmwrap : %s] connected\n",hostname);
    }

  }
  printf("[dsmwrap : %s] All connection made! \n",hostname);

  printf("NAME %s\n",prog_name);
  for (i = 0; i < argc-3; i++) {
    printf("ARGS %s\n",prog_args[i]);
  }

  /* on execute la bonne commande */
  if(execvp(prog_name,prog_args)==-1) perror("exec");

  return EXIT_SUCCESS;
}
