/*
------------------------------------------------------------------------------
DSM project (distributed shared memory) Telecom 2A - Enseirb-mmk
@ Elise Marie & David Pignato
------------------------------------------------------------------------------
*/

#define BACKLOG 5

void get_hostname_ip(char * hostname, char * hostaddr);
int create_socket(int * port_num);
void init_sock_addr(int sockfd, struct sockaddr_in *sock_addr, char * port);
void get_addr_info(const char* address, const char* port, struct addrinfo** res);
int do_socket(int domain, int type, int protocol);
void do_bind(int sockfd, struct sockaddr_in serv_addr);
void do_listen(int sockfd, int max_connection);
void do_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
int do_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
ssize_t readline(int fd, char *str, ssize_t maxlen);
ssize_t sendline(int fd, const char *str, ssize_t maxlen);
