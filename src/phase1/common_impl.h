/*
------------------------------------------------------------------------------
DSM project (distributed shared memory) Telecom 2A - Enseirb-mmk
@ Elise Marie & David Pignato
------------------------------------------------------------------------------
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <netdb.h>


/* autres includes (eventuellement) */
#define _GNU_SOURCE

#define NAME_MAXLEN 64
#define ADDR_MAXLEN 128
#define BUFFLEN 1024

#define ERROR_EXIT(str) {perror(str);exit(EXIT_FAILURE);}

// info of the processus on the local machine
struct local_info{
  pid_t pid;
  int outfd;
  int errfd;
};
typedef struct local_info local_info_t;

// info of the remote machine (ssh by a processus)
struct remote_info{
  pid_t pid;
  int sockfd;
  int port_num;
  char hostaddr[ADDR_MAXLEN];
};
typedef struct remote_info remote_info_t;

struct machine{
  char * name;
  int rank;
  local_info_t local_info;
  remote_info_t remote_info;
};
typedef struct machine machine_t;


int get_num_procs(char* filename);
void get_machine_names(char* filename, machine_t * machine_tab);
