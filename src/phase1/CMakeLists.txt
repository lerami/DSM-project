cmake_minimum_required(VERSION 2.8.3)

project(dsm-phase1)


source_group("Headers" REGULAR_EXPRESSION ".*\\.h$")

set (CMAKE_C_FLAGS "-DREENTRANT -Wunused -Wall -g")

#sub directory of the project dsm-phase1
include_directories(
)


#add here source files you need to compile and link to the app
SET(SRC_DSM
network.c
common.c
dsmexec.c
)

#create executable
add_executable(dsm-phase1 ${SRC_DSM})
find_package(Threads)
target_link_libraries(dsm-phase1 ${CMAKE_THREAD_LIBS_INIT})
