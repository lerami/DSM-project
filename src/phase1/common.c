/*
------------------------------------------------------------------------------
DSM project (distributed shared memory) Telecom 2A - Enseirb-mmk
@ Elise Marie & David Pignato
------------------------------------------------------------------------------
*/

#include "common_impl.h"

int get_num_procs(char* filename){
  int num_procs = 0;
  FILE* machine_file;
	char *buffer = malloc(NAME_MAXLEN);

  machine_file = fopen(filename,"r");
  while(fgets(buffer,NAME_MAXLEN,machine_file) != NULL) num_procs++;

  fclose(machine_file);
  return num_procs;
}

void get_machine_names(char* filename, machine_t * machine_tab){

	FILE* machine_file;
	int i = 0;
	char *buffer = malloc(NAME_MAXLEN);

	machine_file = fopen(filename,"r");
    if(machine_file == NULL){
      perror("open machine_file");
      exit(EXIT_FAILURE);
    }
    while(fgets(buffer,NAME_MAXLEN,machine_file) != NULL)
    {
      machine_tab[i].name = malloc(sizeof(char)*NAME_MAXLEN);
      buffer[strcspn(buffer, "\n")] = '\0'; // drop the '\n'
      strcpy(machine_tab[i].name,buffer); // write the machine name in the tab machines
      machine_tab[i].rank = i;
      i++;
      memset(buffer,0,NAME_MAXLEN);
    }
    fclose(machine_file);
    free(buffer);
}
