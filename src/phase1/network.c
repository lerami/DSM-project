/*
------------------------------------------------------------------------------
DSM project (distributed shared memory) Telecom 2A - Enseirb-mmk
@ Elise Marie & David Pignato
------------------------------------------------------------------------------
*/
#include "common_impl.h"
#include "network.h"

void get_hostname_ip(char * hostname, char * hostaddr){
	struct hostent *host;
	struct in_addr *addr;
	host = gethostbyname(hostname);
	addr = (struct in_addr*) host->h_addr;
	strcpy(hostaddr,inet_ntoa(*addr));
}

// create the socket + address, bind & listen
int create_socket(int * port_num)
{
	struct sockaddr_in sock_addr;
	int sockfd = 0;
	char port[BUFFLEN];

	//create the socket (0 : TCP)
	sockfd = do_socket(AF_INET, SOCK_STREAM, 0);

	sprintf(port,"%d",0);
	// init the sock addr
	init_sock_addr(sockfd, &sock_addr,port);
	//bind the socket
	do_bind(sockfd, sock_addr);

	// listen the socket
	do_listen(sockfd, 5);

	socklen_t sock_addrlen = sizeof(sock_addr);
	if(getsockname(sockfd, (struct sockaddr*)&sock_addr, &sock_addrlen)==-1){
		perror("getsockname");
		exit(EXIT_FAILURE);
	}
	else{
		*port_num = (int)ntohs(sock_addr.sin_port);
	}

	return sockfd;
}

// initialize socket address
void init_sock_addr(int sockfd, struct sockaddr_in *sock_addr, char * port) {
	socklen_t sock_addr_len = sizeof(sock_addr);
	//clean the sock_add structure
	memset(sock_addr, 0, sock_addr_len);
	//internet family protocol
	sock_addr->sin_family =AF_INET;
	//we bind to any ip form the host
	sock_addr->sin_addr.s_addr =INADDR_ANY;

	sock_addr->sin_port = htons(atoi(port));
}

void get_addr_info(const char* address, const char* port, struct addrinfo** res) {

	int status;
	struct addrinfo hints;

	memset(&hints,0,sizeof(hints));

	hints.ai_family=AF_INET;
	hints.ai_socktype=SOCK_STREAM;

	hints.ai_flags = 0;
	hints.ai_protocol = 0;

	status = getaddrinfo(address,port,&hints,res);
	if (status != 0) {
		perror("get_addr_info");
		exit(EXIT_FAILURE);
	}

}

int do_socket(int domain, int type, int protocol) {
	int sockfd;
	int yes = 1;
	//create the socket
	sockfd = socket(domain, type, protocol);

	//check for socket validity
	if(sockfd==-1) {
		fprintf(stderr,"Socket error\n");
		return EXIT_FAILURE;
	}

	// set socket option, to prevent "already in use" issue when rebooting the server right on

	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
	perror("ERROR setting socket options");

	return sockfd;
}

void do_bind(int sockfd, struct sockaddr_in serv_addr) {
	if( bind( sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) == -1) {
		perror("Binding error");
		exit(EXIT_FAILURE);
	}
}

void do_listen(int sockfd, int max_connection) {
	if(listen(sockfd, max_connection)==-1) {
		perror("Listen error");
		exit(EXIT_FAILURE);
	}
}

void do_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
	int res;
	do{
		res = connect(sockfd, addr, addrlen);
	}while ((-1 == res) && (errno == EINTR));

	if (res != 0) {
		perror("Connection error\n");
		exit(EXIT_FAILURE);
	}
}

int do_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen){
	int remote_sockfd;

	do{
		remote_sockfd=accept(sockfd, addr, addrlen);
	}while ((-1 == remote_sockfd) && (errno == EINTR));

	if(remote_sockfd==-1) {
		perror("Accept error");
		return EXIT_FAILURE;
	}

	return remote_sockfd;

}

ssize_t readline(int fd, char *str, ssize_t maxlen){

	int written = 0;
	ssize_t status;
	do{
		status = read(fd, str + written, maxlen - written);
		written=status + written;
	}while((status>0) && (str[written-1]!='\n'));
	return written;
}


ssize_t sendline(int fd, const char *str, ssize_t maxlen){

	int sent=0;

	do{
		sent += write(fd, str + sent, maxlen-sent);
	}while(sent != maxlen);

	return sent;
}
